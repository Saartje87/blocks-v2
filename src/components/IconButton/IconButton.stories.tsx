import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { IconButton, IconButtonProps } from './IconButton';

export default {
  title: 'Button/IconButton',
  component: IconButton,
  argTypes: {},
} as Meta<typeof IconButton>;

const Template: StoryFn<typeof IconButton> = (args) => (
  <>
    <IconButton {...args} />
  </>
);

export const Default: StoryObj<IconButtonProps> = {
  render: Template,

  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    expect(canvas.getByLabelText('IconButton')).toBeInTheDocument();
  },

  args: {
    label: 'IconButton',
    icon: 'arrow-right',
    size: 'medium',
    color: 'primary',
  },
};
