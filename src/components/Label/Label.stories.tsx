import { expect } from '@storybook/jest';
import { Meta, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { Label, LabelProps } from './Label';

export default {
  title: 'Form/Label',
  component: Label,
  argTypes: {},
} as Meta;

export const Default: StoryObj<LabelProps> = {
  render: (props) => {
    return <Label {...props} />;
  },
  play: async ({ canvasElement, args }) => {
    const canvas = within(canvasElement);

    expect(canvas.getByText('Label')).toBeInTheDocument();
  },
  args: {
    children: 'Label',
  },
};
