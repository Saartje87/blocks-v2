import { forwardRef, ReactNode, useId } from 'react';
import { HTMLProps, OptionalLiteral } from '../../types/utils';
import { classnames } from '../../utils';
import { useComponentStyles } from '../BlocksProvider/useComponentStyles';
import { Box } from '../Box';
import { Label } from '../Label/Label';
import * as styles from './Input.css';

export type InputProps = {
  className?: string;
  name: string;
  type?: OptionalLiteral<'email' | 'number' | 'password' | 'tel' | 'text' | 'url'>;
  startSlot?: ReactNode;
  endSlot?: ReactNode;
  label: string;
} & Omit<HTMLProps<HTMLInputElement>, 'type'>;

export const Input = forwardRef<HTMLInputElement, InputProps>(function Input(
  { className, name, type = 'text', startSlot, endSlot, label, placeholder, ...restProps },
  ref,
) {
  const id = useId();
  const containerClassName = useComponentStyles('input', { container: true });
  const inputClassName = useComponentStyles('input', { input: true });

  return (
    <Box>
      <Label htmlFor={id}>{label}</Label>

      <Box display="flex" alignItems="center" className={classnames(containerClassName, className)}>
        {startSlot}

        <Box
          as="input"
          id={id}
          ref={ref}
          name={name}
          type={type}
          placeholder={placeholder || label}
          width="full"
          overflow="hidden"
          className={classnames(styles.input, inputClassName)}
          {...restProps}
        />

        {endSlot}
      </Box>
    </Box>
  );
});
