import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { Box } from '../Box';
import { Spinner, SpinnerProps } from './Spinner';

export default {
  title: 'Feedback/Spinner',
  component: Spinner,
  argTypes: {},
} as Meta<SpinnerProps>;

const Template: StoryFn<SpinnerProps> = (args) => (
  <Box backgroundColor="primaryLight" padding="gutter">
    <Spinner {...args} />
  </Box>
);

export const Default: StoryObj<SpinnerProps> = {
  render: Template,

  args: {
    size: 'medium',
  },
};
