import { style } from '@vanilla-extract/css';
import { createComponentTheme } from '../../components/BlocksProvider/createComponentTheme';
import { bounceOut } from './transitions';

export const radio = createComponentTheme('radio', ({ atoms }) => ({
  radio: style([
    {
      transition: 'background-color 120ms linear',
    },
    atoms({
      backgroundColor: 'primaryLight',
    }),
    style({
      width: 24,
      height: 24,
      borderRadius: '50%',
    }),
  ]),
  icon: style({
    transition: `transform 160ms ${bounceOut}`,
  }),
}));
