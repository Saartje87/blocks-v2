import { FC, ReactNode } from 'react';
import { Atoms } from '../../css/sprinkles.css';
import { HTMLProps } from '../../types/utils';
import { PaddingAtoms } from '../../utils/css';
import { Box } from '../Box';

export type HeadingProps = {
  className?: string;
  level: 1 | 2 | 3 | 4 | 5 | 6;
  children: ReactNode;
  align?: Atoms['textAlign'];
  fontSize?: Atoms['fontSize'];
  fontWeight?: Atoms['fontWeight'];
  fontFamily?: Atoms['fontFamily'];
} & PaddingAtoms &
  HTMLProps<HTMLHeadingElement>;

export const Heading: FC<HeadingProps> = ({
  className,
  level = 1,
  children,
  align,
  fontSize,
  fontWeight = 'strong',
  fontFamily,
  ...restProps
}) => {
  const as = `h${level}` as const;

  return (
    <Box
      as={as}
      className={className}
      fontFamily={fontFamily}
      fontWeight={fontWeight}
      fontSize={fontSize}
      textAlign={align}
      padding="none"
      margin="none"
      {...restProps}
    >
      {children}
    </Box>
  );
};
