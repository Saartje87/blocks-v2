import { expect } from '@storybook/jest';
import { Meta, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { Text } from '../Text';
import { Link, LinkProps } from './Link';

export default {
  title: 'Navigation/Link',
  component: Link,
  argTypes: {},
} as Meta<typeof Link>;

export const Default = {
  args: {
    children: 'Link',
  },
};

export const WithText: StoryObj<LinkProps> = {
  render: (args) => (
    <Text as="p">
      Lorem <Link {...args} /> dolor sit amet, consectetur adipiscing elit. Cras lectus turpis,
      scelerisque eu odio id, pretium molestie erat.
    </Text>
  ),

  args: {
    children: 'Ipsum',
  },
};

export const Play: StoryObj<LinkProps> = {
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    expect(canvas.getByText('Link')).toBeInTheDocument();
  },

  args: {
    children: 'Link',
  },
};
