import { style } from '@vanilla-extract/css';
import { createComponentTheme } from '../../components/BlocksProvider/createComponentTheme';
import { bounceOut } from './transitions';

export const checkbox = createComponentTheme('checkbox', ({ atoms }) => ({
  checkbox: style([
    {
      transition: 'background-color 120ms linear',
    },
    atoms({
      color: 'white',
      backgroundColor: 'primaryLight',
      borderRadius: 'xsmall',
    }),
    style({
      width: 24,
      height: 24,
    }),
  ]),
  icon: style({
    transition: `transform 160ms ${bounceOut}`,
  }),
}));
