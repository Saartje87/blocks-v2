// Move to other folder
import { atoms } from '../../css/sprinkles.css';
import { vars } from '../../css/theme.css';
import { ComponentTheme } from '../../types/componentTheme';

type Options = {
  vars: typeof vars;
  atoms: typeof atoms;
};

export const createComponentTheme = <T extends keyof ComponentTheme>(
  component: T,
  callback: (props: Options) => ComponentTheme[T],
): ComponentTheme[T] => {
  return {
    component,
    ...callback({ vars, atoms }),
  };
};
