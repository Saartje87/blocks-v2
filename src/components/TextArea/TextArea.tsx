import { forwardRef, useEffect, useId, useRef } from 'react';
import { useIsomorphicLayoutEffect } from '../../hooks';
import { HTMLProps } from '../../types/utils';
import { classnames } from '../../utils';
import { mergeRefs } from '../../utils/mergeRefs';
import { useComponentStyles } from '../BlocksProvider';
import { Box } from '../Box';
import { Label } from '../Label/Label';
import * as styles from './textarea.css';

export type TextAreaProps = {
  children?: string;
  label?: string;
  autoGrow?: boolean;
  maxHeight?: number | string;
} & HTMLProps<HTMLTextAreaElement>;

export const TextArea = forwardRef<HTMLTextAreaElement, TextAreaProps>(function TextArea(
  { children, label, onInput, autoGrow, maxHeight, ...restProps },
  ref,
) {
  const id = useId();
  const inputRef = useRef<HTMLTextAreaElement>(null);
  const containerRef = useRef<HTMLDivElement>(null);
  // TODO - useComponentStyles('textarea', { xxx })
  const containerClassName = useComponentStyles('input', { container: true });
  const inputClassName = useComponentStyles('input', { input: true });

  // Set initial "content" of the container to the value of the input
  useIsomorphicLayoutEffect(() => {
    if (!inputRef.current || !containerRef.current) {
      return;
    }

    if (autoGrow) {
      containerRef.current.dataset.replicatedValue = inputRef.current.value;
    } else {
      containerRef.current.dataset.replicatedValue = '';
    }
  }, [autoGrow]);

  // Add event listener to the input to update the container's "content"
  // so that the container can grow/shrink to match the input's content
  useEffect(() => {
    const onInput = () => {
      if (!containerRef.current || !inputRef.current || !autoGrow) {
        return;
      }

      containerRef.current.dataset.replicatedValue = inputRef.current.value;
    };

    inputRef.current?.addEventListener('input', onInput);

    return () => {
      inputRef.current?.removeEventListener('input', onInput);
    };
  }, [autoGrow]);

  return (
    <Box>
      <Label htmlFor={id}>{label}</Label>

      <Box
        ref={containerRef}
        display="grid"
        overflow="hidden"
        className={classnames(containerClassName, styles.container)}
        style={{ maxHeight }}
      >
        <Box
          as="textarea"
          ref={mergeRefs(ref, inputRef)}
          id={id}
          className={classnames(
            styles.textarea,
            inputClassName,
            !!(maxHeight && autoGrow) && styles.autoGrowMaxHeight,
          )}
          {...restProps}
        >
          {children}
        </Box>
      </Box>
    </Box>
  );
});
