import { FC, ReactNode } from 'react';
import { Atoms } from '../../css/sprinkles.css';
import { HTMLProps } from '../../types/utils';
import { PaddingAtoms } from '../../utils/css';
import { Box } from '../Box/Box';

export type TextProps = {
  children: ReactNode;
  as?: 'span' | 'p' | 'strong' | 'em' | 'small' | 's' | 'del' | 'ins' | 'sub' | 'sup';
  color?: Atoms['color'];
  fontSize?: Atoms['fontSize'];
  fontWeight?: Atoms['fontWeight'];
  fontFamily?: Atoms['fontFamily'];
  textAlign?: Atoms['textAlign'];
  fontStyle?: Atoms['fontStyle'];
  textDecoration?: Atoms['textDecoration'];
  lineHeight?: Atoms['lineHeight'];
} & PaddingAtoms &
  HTMLProps<HTMLSpanElement>;

export const Text: FC<TextProps> = ({
  as = 'span',
  children,
  color,
  fontSize,
  fontWeight,
  fontFamily,
  textAlign,
  ...restProps
}) => {
  return (
    <Box
      as={as}
      color={color}
      fontSize={fontSize}
      fontWeight={fontWeight}
      fontFamily={fontFamily}
      textAlign={textAlign}
      padding="none"
      margin="none"
      {...restProps}
    >
      {children}
    </Box>
  );
};
