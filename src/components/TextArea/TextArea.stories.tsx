import { Meta, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';
import { TextArea, TextAreaProps } from './TextArea';

export default {
  title: 'Form/TextArea',
  component: TextArea,
  argTypes: {},
} as Meta<typeof TextArea>;

export const Default: StoryObj<TextAreaProps> = {
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    userEvent.type(canvas.getByLabelText('WockleBender'), 'Wockle');
  },

  args: {
    name: 'foo',
    label: 'WockleBender',
    placeholder: 'TextArea a value',
    required: true,
    rows: 3,
  },
};

export const AutoGrow: StoryObj<TextAreaProps> = {
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    await userEvent.type(
      canvas.getByLabelText('AutoGrow'),
      'WockleBender.\n\nWockleBender?\n\nWockleBender!',
      {
        delay: 20,
      },
    );
  },

  args: {
    name: 'foo',
    label: 'AutoGrow',
    placeholder: 'TextArea a value',
    required: true,
    autoGrow: true,
    // maxHeight: 200,
  },
};
