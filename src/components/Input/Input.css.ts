import { style } from '@vanilla-extract/css';
import { vars } from '../../css/theme.css';

export const input = style({
  alignSelf: 'stretch',
  appearance: 'none',
  border: 'none',
  '::placeholder': {
    color: vars.color.textLight,
  },
});
