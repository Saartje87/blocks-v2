import { style } from '@vanilla-extract/css';
import { createComponentTheme } from '../../components/BlocksProvider/createComponentTheme';

export const progress = createComponentTheme('progress', ({ atoms }) => ({
  container: style([
    atoms({
      borderRadius: 'small',
    }),
    {
      height: '4px',
    },
  ]),
  bar: style({}),
}));
