import { ButtonProps } from '../components/Button';
import { LinkProps } from '../components/Link';

type ThemeRecord<T extends string> = Record<T, string>;

export type ComponentTheme = {
  button: {
    base: string;
    // Link variant is excluded because styles are inherited from the link component
    variant: ThemeRecord<Exclude<ButtonProps['variant'], undefined | 'link'>>;
    intent: ThemeRecord<Exclude<ButtonProps['intent'], undefined>>;
    size: ThemeRecord<Exclude<ButtonProps['size'], undefined>>;
  };
  dialog: {
    base: string;
    backdrop: string;
  };
  link: {
    base: string;
    variant: ThemeRecord<Exclude<LinkProps['variant'], undefined>>;
  };
  progress: {
    container: string;
    bar: string;
  };
  checkbox: {
    checkbox: string;
    icon: string;
  };
  radio: {
    radio: string;
    icon: string;
  };
  label: {
    base: string;
  };
  input: {
    container: string;
    input: string;
  };
  iconButton: {
    base: string;
  };
  spinner: {
    size: ThemeRecord<'small' | 'medium' | 'large'>;
  };
};

export type FlattenComponentTheme<T extends ComponentTheme[keyof ComponentTheme]> = {
  [P in keyof T]: T[P] extends Record<string, unknown> ? keyof T[P] : boolean;
};

export type ComponentThemeOptions<T extends keyof ComponentTheme> = Partial<
  FlattenComponentTheme<ComponentTheme[T]>
>;

export type Theme = {
  vars: string;
} & ComponentTheme;
