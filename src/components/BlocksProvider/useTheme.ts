import { Theme } from '../../types/componentTheme';
import { useBlocksContext } from './useBlocksContext';

export const useTheme = (): Theme => {
  const { theme } = useBlocksContext();

  return theme;
};
