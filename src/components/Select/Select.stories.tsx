import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';
import { Select, SelectProps } from './Select';

export default {
  title: 'Form/Select',
  component: Select,
  argTypes: {},
} as Meta<typeof Select>;

const Template: StoryFn<typeof Select> = (args) => (
  <Select {...args}>
    <option value="A">A</option>
    <option value="B">B</option>
  </Select>
);

export const Default: StoryObj<SelectProps> = {
  render: Template,

  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    userEvent.selectOptions(canvas.getByLabelText('WockleBender'), 'B');
  },

  args: {
    name: 'foo',
    label: 'WockleBender',
    placeholder: 'Select a value',
    required: true,
  },
};
