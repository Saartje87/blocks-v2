import { zeroSpecificityStyle } from '../../utils/zeroSpecificityStyle.css';

export const buttonReset = zeroSpecificityStyle({
  all: 'unset',
  cursor: 'pointer',
});
