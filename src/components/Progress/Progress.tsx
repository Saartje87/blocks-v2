import { FC } from 'react';
import { Atoms } from '../../css/sprinkles.css';
import { classnames } from '../../utils';
import { useComponentStyles } from '../BlocksProvider/useComponentStyles';
import { Box } from '../Box';
import * as styles from './progress.css';

export type ProgressBarProps = {
  /**
   * The value of the progress bar, between 0 and 100.
   */
  value: number;
  backgroundColor?: Atoms['backgroundColor'];
  barColor?: Atoms['backgroundColor'];
  className?: string;
  speed?: 'none' | keyof typeof styles.transitions;
  'aria-labelledby'?: string;
};

export const ProgressBar: FC<ProgressBarProps> = ({
  value,
  backgroundColor = 'textLight',
  barColor = 'primary',
  className,
  speed = 'fast',
  ...restProps
}) => {
  const containerClassName = useComponentStyles('progress', { container: true });
  const barClassName = useComponentStyles('progress', { bar: true });
  const transition = speed === 'none' ? undefined : styles.transitions[speed];

  return (
    <Box
      role="progressbar"
      aria-valuenow={value}
      aria-valuemin={0}
      aria-valuemax={100}
      backgroundColor={backgroundColor}
      width="full"
      overflow="hidden"
      className={classnames(containerClassName, className)}
      {...restProps}
    >
      <Box
        backgroundColor={barColor}
        width="full"
        height="full"
        className={classnames(barClassName, transition)}
        style={{ transform: `translateX(-${100 - value}%)` }}
      />
    </Box>
  );
};
