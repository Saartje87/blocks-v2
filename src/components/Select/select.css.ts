import { style } from '@vanilla-extract/css';
import { calc } from '@vanilla-extract/css-utils';
import { vars } from '../../css/theme.css';

export const select = style({
  height: '100%',
  appearance: 'none',
  border: 'none',
  flexGrow: 1,
  // TODO: paddingRight should be set in component theme
  paddingRight: calc.multiply(vars.space.xlarge, 2),
  ':invalid': {
    color: vars.color.textLight,
  },
});

export const placeholder = style({
  color: vars.color.textLight,
});
