import { globalStyle, GlobalStyleRule, style } from '@vanilla-extract/css';

/**
 * Returns a class name that can be used to apply a global style rule with zero specificity.
 * This is useful for resetting styles on native elements.
 */
export const zeroSpecificityStyle = (rule: GlobalStyleRule) => {
  const className = style({});

  globalStyle(`:where(${className})`, rule);

  return className;
};
