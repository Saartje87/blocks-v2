import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { getByPlaceholderText, getByRole, userEvent, within } from '@storybook/testing-library';
import { FC, ReactNode, useEffect, useState } from 'react';
import { flushSync } from 'react-dom';
import { Button } from '../Button';
import { Heading } from '../Heading';
import { Input } from '../Input';
import { Stack } from '../Stack';
import { Text } from '../Text';
import { Dialog, DialogProps } from './Dialog';

const sleep = (time: number) => new Promise((resolve) => setTimeout(resolve, time));

const NestedDialog: FC<{ children?: ReactNode }> = ({ children }) => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Button onClick={() => setOpen(true)}>Open</Button>
      <Dialog open={open} onRequestClose={() => setOpen(false)}>
        {children}
      </Dialog>
    </>
  );
};

export default {
  title: 'Overlay/Dialog',
  component: Dialog,
  argTypes: {
    children: {
      control: {
        type: 'none',
      },
    },
    header: {
      control: {
        type: 'none',
      },
    },
    footer: {
      control: {
        type: 'none',
      },
    },
  },
} as Meta<typeof Dialog>;

const Template: StoryFn<typeof Dialog> = (args) => {
  const [open, setOpen] = useState(args.open);

  useEffect(() => {
    setOpen(args.open);
  }, [args.open]);

  return (
    <>
      <Button
        variant="solid"
        onClick={() => {
          flushSync(() => {
            setOpen(true);
          });
        }}
      >
        Open Dialog
      </Button>
      <div style={{ height: '2000px' }}></div>
      <Dialog
        {...args}
        open={open}
        onRequestClose={() =>
          flushSync(() => {
            setOpen(false);
          })
        }
      />
    </>
  );
};

export const Default = {
  render: Template,
  args: {
    children: (
      <>
        <Stack gap="medium">
          <Heading level={2}>Hello world!</Heading>
          <Text as="p" fontSize="small">
            This is a dialog.
          </Text>
          <NestedDialog>
            One
            <NestedDialog>Two</NestedDialog>
          </NestedDialog>
          <form onSubmit={(event) => event.preventDefault()}>
            <Stack gap="small" alignX="center">
              <Input name="firstName" label="First name" autoFocus />
              <Button type="submit">Submit</Button>
            </Stack>
          </form>
        </Stack>
      </>
    ),
    open: false,
  },
};

export const Play: StoryObj<DialogProps> = {
  render: Template,
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    expect(canvas.getByRole('button', { name: 'Open Dialog' })).toBeInTheDocument();
    userEvent.click(canvas.getByRole('button', { name: 'Open Dialog' }));

    await sleep(30);

    expect(getByRole(document.body, 'dialog')).toBeInTheDocument();
    expect(getByPlaceholderText(document.body, 'First name')).toBeInTheDocument();
  },
  args: {
    children: (
      <>
        <Stack gap="medium">
          <Heading level={2}>Hello world!</Heading>
          <Text as="p" fontSize="small">
            This is a dialog.
          </Text>
          <NestedDialog>
            One
            <NestedDialog>Two</NestedDialog>
          </NestedDialog>
          <form onSubmit={(event) => event.preventDefault()}>
            <Stack gap="small" alignX="center">
              <Input name="firstName" label="First name" autoFocus />
              <Button type="submit">Submit</Button>
            </Stack>
          </form>
        </Stack>
      </>
    ),
    open: false,
  },
};
