import { FC } from 'react';
import { HTMLProps } from '../../types/utils';
import { classnames } from '../../utils';
import { useComponentStyles } from '../BlocksProvider';
import { Box } from '../Box';

export type LabelProps = HTMLProps<HTMLLabelElement>;

export const Label: FC<LabelProps> = ({ className, children, ...restProps }) => {
  const labelClassName = useComponentStyles('label', { base: true });

  return (
    <Box as="label" className={classnames(className, labelClassName)} {...restProps}>
      {children}
    </Box>
  );
};
