import { FC, ReactNode } from 'react';
import { HTMLProps } from '../../types/utils';
import { classnames } from '../../utils';
import { useComponentStyles } from '../BlocksProvider/useComponentStyles';
import { Box } from '../Box';
import { Icon } from '../Icon';
import { Text } from '../Text';
import * as styles from './Checkbox.css';

export type CheckboxProps = {
  className?: string;
  name: string;
  label: ReactNode;
} & HTMLProps<HTMLInputElement>;

export const Checkbox: FC<CheckboxProps> = ({ className, name, label, ...restProps }) => {
  const checkboxClassName = useComponentStyles('checkbox', { checkbox: true });
  const iconClassName = useComponentStyles('checkbox', { icon: true });

  return (
    <Box as="label" display="flex" alignItems="center" className={className}>
      <Box marginRight="medium">
        <input type="checkbox" name={name} className={styles.input} {...restProps} />
        <Box
          cursor="pointer"
          display="flex"
          placeItems="center"
          className={classnames(styles.checkbox, checkboxClassName)}
        >
          <Icon
            icon="check"
            size="small"
            className={classnames(iconClassName, styles.checkboxMark)}
          />
        </Box>
      </Box>
      <Text>{label}</Text>
    </Box>
  );
};
