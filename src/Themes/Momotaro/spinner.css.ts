import { styleVariants } from '@vanilla-extract/css';
import { createComponentTheme } from '../../components/BlocksProvider/createComponentTheme';

export const spinner = createComponentTheme('spinner', ({ atoms }) => ({
  size: styleVariants({
    small: {
      width: 16,
    },
    medium: {
      width: 24,
    },
    large: {
      width: 32,
    },
  }),
}));
