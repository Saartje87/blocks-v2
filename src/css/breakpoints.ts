export const breakpoints = {
  mobile: 0,
  tablet: 740,
  desktop: 992,
  wide: 1200,
} as const;

export type BreakpointKeys = keyof typeof breakpoints;

export const breakpointKeys = Object.keys(breakpoints) as BreakpointKeys[];

export const breakpointQuery = (key: BreakpointKeys) =>
  `screen and (min-width: ${breakpoints[key]}px)`;
