import { style } from '@vanilla-extract/css';
import { vars } from '../../css/theme.css';

export const focusable = style({
  ':focus-visible': {
    outline: '2px solid transparent',
    outlineOffset: '2px',
    boxShadow: vars.shadow.focus,
    transitionDuration: vars.transition.duration.normal,
    transitionProperty: 'box-shadow',
  },
  ':disabled': {
    opacity: 0.5,
    cursor: 'auto',
  },
  // TODO This is for buttons?
  selectors: {
    '&:active:not(:disabled)': {
      transform: 'scale(0.9)',
    },
  },
});
