import { FC, ReactNode } from 'react';
import { HTMLProps } from '../../types/utils';
import { classnames } from '../../utils';
import { useComponentStyles } from '../BlocksProvider/useComponentStyles';
import { Box } from '../Box';
import { Text } from '../Text';
import * as styles from './Radio.css';

export type RadioProps = {
  className?: string;
  name: string;
  label: ReactNode;
} & HTMLProps<HTMLInputElement>;

export const Radio: FC<RadioProps> = ({ className, name, label, ...restProps }) => {
  const radioClassName = useComponentStyles('radio', { radio: true });
  const iconClassName = useComponentStyles('radio', { icon: true });

  return (
    <Box as="label" display="flex" alignItems="center" className={className}>
      <input type="radio" name={name} className={styles.input} {...restProps} />
      <Box
        cursor="pointer"
        display="flex"
        placeItems="center"
        marginRight="medium"
        className={classnames(styles.radio, radioClassName)}
      >
        <div className={classnames(styles.radioMark, iconClassName)} />
      </Box>

      <Text>{label}</Text>
    </Box>
  );
};
