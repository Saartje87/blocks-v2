import { FC } from 'react';
import { Atoms } from '../../css/sprinkles.css';
import { classnames } from '../../utils';
import { useBlocksContext } from '../BlocksProvider/useBlocksContext';
import { Box } from '../Box';
import * as styles from './Icon.css';
import { IconNames } from './iconNames';

export type IconProps = {
  icon: IconNames;
  size?: keyof typeof styles.sizes;
  className?: string;
  color?: Atoms['color'];
  label?: string;
};

export const Icon: FC<IconProps> = ({
  icon,
  className,
  size = 'medium',
  color = 'currentColor',
  label,
}) => {
  const { spriteUrl } = useBlocksContext();

  return (
    <Box
      as="svg"
      viewBox="0 0 100 100"
      color={color}
      className={classnames(styles.sizes[size], className)}
      role={label ? undefined : 'presentation'}
      aria-label={label}
    >
      <use className={styles.iconColor} href={`${spriteUrl}#${icon}`} />
    </Box>
  );
};
