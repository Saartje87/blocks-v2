import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';
import { Stack } from '../Stack';
import { Input, InputProps } from './Input';

export default {
  title: 'Form/Input',
  component: Input,
  argTypes: {},
} as Meta<typeof Input>;

const Template: StoryFn<typeof Input> = (args) => <Input {...args} />;

export const Default: StoryObj<InputProps> = {
  render: Template,

  args: {
    name: 'name',
    type: 'name',
    label: 'Your label here',
  },
};

export const Play: StoryObj<InputProps> = {
  render: (args) => (
    <Stack gap="large">
      <Input {...args} />
      <Input {...args} label="First name" />
      <Input {...args} label="Last name" />
      <Input {...args} label="Password" type="password" endSlot="👀" />
      <Input {...args} label="Disabled" disabled startSlot="📙" />
    </Stack>
  ),

  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    await userEvent.type(canvas.getByLabelText('Email'), 'email@provider.com', {
      delay: 20,
    });
  },

  args: {
    name: 'email',
    type: 'email',
    label: 'Email',
  },
};
