import { keyframes, style } from '@vanilla-extract/css';

const spinAnimation = keyframes({
  '0%': {
    transform: 'rotate(0deg)',
  },
  '100%': {
    transform: 'rotate(360deg)',
  },
});

export const spinner = style({
  aspectRatio: '1 / 1',
  overflow: 'hidden',
  borderRadius: '50%',
  borderWidth: '3px',
  borderStyle: 'solid',
  borderColor: 'currentColor transparent currentColor transparent',
  animation: `${spinAnimation} 1.2s linear infinite`,
});
