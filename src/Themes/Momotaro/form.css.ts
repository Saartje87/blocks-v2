import { style } from '@vanilla-extract/css';
import { createComponentTheme } from '../../components/BlocksProvider/createComponentTheme';

export const label = createComponentTheme('label', ({ atoms }) => ({
  base: style([
    atoms({
      display: 'block',
      color: 'text',
      paddingX: 'medium',
      marginBottom: 'small',
    }),
  ]),
}));

export const input = createComponentTheme('input', ({ atoms, vars }) => ({
  container: style([
    atoms({
      backgroundColor: 'white',
      borderRadius: 'medium',
      boxShadow: 'medium',
      gap: 'large',
    }),
    style({
      minHeight: 56,
      transitionDuration: vars.transition.duration.normal,
      transitionProperty: 'box-shadow',
      ':focus-within': {
        outline: '2px solid transparent',
        outlineOffset: '2px',
        boxShadow: vars.shadow.focus,
      },
    }),
  ]),
  input: style([
    atoms({
      color: 'text',
      paddingY: 'large',
      paddingX: 'large',
    }),
    {
      outline: 'none',
      background: 'transparent',
    },
  ]),
}));
