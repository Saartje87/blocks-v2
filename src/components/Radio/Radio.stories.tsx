import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { Stack } from '../Stack';
import { Radio, RadioProps } from './Radio';

export default {
  title: 'Form/Radio',
  component: Radio,
  argTypes: {},
} as Meta<typeof Radio>;

const Template: StoryFn<typeof Radio> = (args) => (
  <fieldset>
    <Stack gap="medium">
      <Radio {...args} name="foo" />
      <Radio label="Radio 2" name="foo" />
      <Radio label="Radio 3" name="foo" />
    </Stack>
  </fieldset>
);

export const Default: StoryObj<RadioProps> = {
  render: Template,

  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    expect(canvas.getByText('Radio 1')).toBeInTheDocument();
  },

  args: {
    label: 'Radio 1',
  },
};
