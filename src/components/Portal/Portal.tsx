import { FC, ReactNode } from 'react';
import { createPortal } from 'react-dom';
import { getRootElement } from '../../utils/rootElement';
import { BlocksProvider } from '../BlocksProvider/BlocksProvider';
import { useBlocksContext } from '../BlocksProvider/useBlocksContext';

export type PortalProps = {
  children: ReactNode;
  container?: HTMLElement;
};

export const Portal: FC<PortalProps> = ({ children, container }) => {
  const context = useBlocksContext();

  // Wrapped in fragment to avoid typescript warning that createPortal is not a valid return for FC
  return (
    <>
      {createPortal(
        <BlocksProvider {...context}>{children}</BlocksProvider>,
        container || getRootElement(),
      )}
    </>
  );
};
