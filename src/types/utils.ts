import { HTMLProps as ReactHTMLProps } from 'react';
import { Atoms } from '../css/sprinkles.css';

export type HTMLProps<E extends Element> = Omit<ReactHTMLProps<E>, keyof Atoms | 'as' | 'ref'>;

export type DeepPartial<T> = T extends object
  ? {
      [P in keyof T]?: DeepPartial<T[P]>;
    }
  : T;

export type EmptyObject = {};

/**
 * Pretty object type when viewing a type in the editor.
 */
export type Pretty<T extends Record<string, unknown>> = {
  [K in keyof T]: T[K];
} & {};

/**
 * Suggest a type for a string literal but also allow any string.
 */
export type OptionalLiteral<T extends string> = T | (string & {});
