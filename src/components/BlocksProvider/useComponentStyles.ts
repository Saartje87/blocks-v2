// Move to other folder
import { ComponentTheme, ComponentThemeOptions } from '../../types/componentTheme';
import { classnames } from '../../utils/classnames';
import { useTheme } from './useTheme';

export const useComponentStyles = <T extends keyof ComponentTheme>(
  component: T,
  options: ComponentThemeOptions<T>,
): string | undefined => {
  const theme = useTheme();
  const componentStyles = theme[component];

  if (!componentStyles) {
    // Should be a warning, in dev mode only?
    if (process.env.NODE_ENV !== 'production') {
      console.warn(`Component styles not found '${component}'`);
    }

    return;
  }

  const classNames: string[] = [];

  for (const key in options) {
    const styles = componentStyles[key] as string | Record<string, string>;
    const value = options[key] as boolean | string;

    if (!styles || !value) {
      continue;
    }

    // If styles is a string then value is a boolean and visa versa
    if (typeof styles === 'string' || typeof value === 'boolean') {
      if (value === true) {
        classNames.push(styles as string);
      }

      continue;
    }

    if (value in styles) {
      classNames.push(styles[value as keyof typeof styles]);
    }
  }

  return classnames(...classNames);
};
