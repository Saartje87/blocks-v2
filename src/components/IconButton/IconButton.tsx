import { FC } from 'react';
import { HTMLProps } from '../../types/utils';
import { classnames } from '../../utils';
import { useComponentStyles } from '../BlocksProvider/useComponentStyles';
import { Box } from '../Box';
import { Icon, IconProps } from '../Icon/Icon';
import * as styles from './IconButton.css';

export type IconButtonProps = {
  icon: IconProps['icon'];
  size?: IconProps['size'];
  color?: IconProps['color'];
  label: string;
  disabled?: boolean;
} & Omit<HTMLProps<HTMLButtonElement>, 'size' | 'type'>;

export const IconButton: FC<IconButtonProps> = ({ icon, size, color, label, ...props }) => {
  const iconButtonClassName = useComponentStyles('iconButton', { base: true });

  return (
    <Box
      as="button"
      type="button"
      padding="small"
      className={classnames(styles.iconButton, iconButtonClassName)}
      aria-label={label}
      {...props}
    >
      <Icon icon={icon} size={size} color={color} />
    </Box>
  );
};
