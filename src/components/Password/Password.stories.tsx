import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { Password, PasswordProps } from './Password';

export default {
  title: 'Form/Password',
  component: Password,
  argTypes: {},
} as Meta<PasswordProps>;

const Template: StoryFn<PasswordProps> = (args) => (
  <>
    <Password {...args} />
  </>
);

export const Default: StoryObj<PasswordProps> = {
  render: Template,

  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    expect(canvas.getByText('Password')).toBeInTheDocument();
  },

  args: {
    label: 'Password',
    value: '123',
  },
};
