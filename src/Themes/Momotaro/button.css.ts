import { createVar, style, styleVariants } from '@vanilla-extract/css';
import { createComponentTheme } from '../../components/BlocksProvider/createComponentTheme';
import { focusable } from './utils.css';

const primaryColor = createVar();

export const button = createComponentTheme('button', ({ atoms, vars }) => ({
  // Button base styles
  base: style([
    atoms({
      display: 'inline-flex',
      placeItems: 'center',
      fontSize: 'medium',
      borderRadius: 'medium',
      fontWeight: 'medium',
    }),
    focusable,
  ]),
  // Button variants
  variant: styleVariants({
    solid: [
      atoms({
        color: 'white',
      }),
      {
        backgroundColor: primaryColor,
        border: 'none',
        selectors: {
          '&:hover:not(:disabled)': {
            backgroundColor: vars.color.primaryDark,
          },
        },
      },
    ],
    outline: [
      {
        color: primaryColor,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: primaryColor,
        background: 'transparent',
        selectors: {
          '&:hover:not(:disabled)': {
            backgroundColor: vars.color.primaryLight,
          },
        },
      },
    ],
    ghost: {
      color: primaryColor,
      background: 'transparent',
      selectors: {
        '&:hover:not(:disabled)': {
          backgroundColor: vars.color.primaryLight,
        },
      },
    },
  }),
  // Button intent
  intent: styleVariants({
    default: {
      vars: {
        [primaryColor]: vars.color.primary,
      },
    },
    warning: {
      vars: {
        [primaryColor]: vars.color.caution,
      },
    },
  }),
  // Button size
  size: styleVariants({
    small: [
      atoms({
        paddingX: 'large',
      }),
      {
        height: 40,
      },
    ],
    medium: [
      atoms({
        paddingX: 'xlarge',
      }),
      {
        height: 56,
      },
    ],
    large: [
      atoms({
        paddingX: 'xlarge',
      }),
      {
        height: 72,
      },
    ],
  }),
}));
