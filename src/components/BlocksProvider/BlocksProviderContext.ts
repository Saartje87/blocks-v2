import { ComponentType, createContext } from 'react';
import { Theme } from '../../types/componentTheme';

export type BlocksProviderContextValue = {
  theme: Theme;
  spriteUrl: string;
  linkComponent?: ComponentType<any>;
};

export const BlocksProviderContext = createContext<BlocksProviderContextValue | null>(null);
