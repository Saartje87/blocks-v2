import { style } from '@vanilla-extract/css';
import { vars } from '../../css/theme.css';

export const container = style({
  overflow: 'auto',
  ':after': {
    content: 'attr(data-replicated-value) " "',
    /* This is how textarea text behaves */
    whiteSpace: 'pre-wrap',
    visibility: 'hidden',
    gridArea: '1 / 1 / 2 / 2',
    // TODO Fix me! This is a hack to make the textarea grow to the correct height
    padding: vars.space.large,
  },
});

export const textarea = style({
  background: 'transparent',
  resize: 'none',
  border: 'none',
  outline: 'none',
  height: 'inherit',
  gridArea: '1 / 1 / 2 / 2',
  overflow: 'visible',
});

export const autoGrowMaxHeight = style({
  overflow: 'hidden',
});
