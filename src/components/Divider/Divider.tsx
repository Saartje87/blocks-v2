import { FC } from 'react';
import { Atoms } from '../../css/sprinkles.css';
import { classnames } from '../../utils';
import { Box } from '../Box';
import * as styles from './Divider.css';

export type DividerProps = {
  className?: string;
  color?: Atoms['backgroundColor'];
};

export const Divider: FC<DividerProps> = ({ className, color = 'textLight' }) => {
  return (
    <Box
      role="separator"
      width="full"
      backgroundColor={color}
      className={classnames(className, styles.divider)}
    />
  );
};
