import { Theme } from '../../types/componentTheme';
import { button } from './button.css';
import { checkbox } from './checkbox.css';
import { dialog } from './dialog.css';
import { input, label } from './form.css';
import { iconButton } from './iconButton.css';
import { link } from './link.css';
import { progress } from './progress.css';
import { radio } from './radio.css';
import { spinner } from './spinner.css';
import { themeVars as vars } from './theme.css';

export const theme: Theme = {
  vars,
  button,
  checkbox,
  dialog,
  iconButton,
  label,
  link,
  progress,
  radio,
  input,
  spinner,
};
