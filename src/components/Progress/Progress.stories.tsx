import { expect } from '@storybook/jest';
import { Meta, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { Stack } from '../Stack';
import { Text } from '../Text';
import { ProgressBar, ProgressBarProps } from './Progress';

export default {
  title: 'Feedback/Progress',
  component: ProgressBar,
  argTypes: {
    value: {
      name: 'Value',
      description: 'Progress bar value',
      control: { type: 'range', min: 0, max: 100, step: 1 },
    },
  },
} as Meta<typeof ProgressBar>;

export const Default: StoryObj<ProgressBarProps> = {
  render: (args) => (
    <Stack gap="medium">
      <Text id={args['aria-labelledby']}>Progress bar label</Text>
      <ProgressBar aria-labelledby={args['aria-labelledby']} {...args} />
    </Stack>
  ),
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    expect(canvas.getByText('Progress bar label')).toBeInTheDocument();
  },

  args: {
    value: 50,
    'aria-labelledby': 'progress-bar',
  },
};
