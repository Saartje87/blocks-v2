import { createThemeContract } from '@vanilla-extract/css';

export const vars = createThemeContract({
  space: {
    none: null,
    gutter: null,
    xsmall: null,
    small: null,
    medium: null,
    large: null,
    xlarge: null,
  },
  color: {
    white: null,
    black: null,
    body: null,
    primaryLight: null,
    primary: null,
    primaryDark: null,
    secondaryLight: null,
    secondary: null,
    secondaryDark: null,
    text: null,
    textLight: null,
    textDark: null,
    caution: null,
    link: null,
  },
  fontFamily: {
    body: null,
    primary: null,
    secondary: null,
  },
  fontSize: {
    // large | regular | small | tiny
    xsmall: null,
    small: null,
    medium: null,
    large: null,
    xlarge: null,
  },
  lineHeight: {
    small: null,
    medium: null,
    large: null,
  },
  weight: {
    medium: null,
    regular: null,
    strong: null,
  },
  border: {
    styles: {
      none: null,
      black: null,
    },
    radius: {
      xsmall: null,
      small: null,
      medium: null,
      large: null,
    },
  },
  shadow: {
    small: null,
    medium: null,
    large: null,
    focus: null,
  },
  // outline: {
  //   focus: null,
  //   offset: null,
  // },
  icon: {
    size: {
      small: null,
      medium: null,
      large: null,
    },
  },
  // heading: {
  //   base: {
  //     fontFamily: 'standard',
  //   },
  // },
  transition: {
    duration: {
      slow: null,
      normal: null,
      fast: null,
    },
    // timing: {
    //   ease: null,
    //   easeIn: null,
    //   easeOut: null,
    // },
  },
  // transform: {}
  // animation: {
  //   dialogEnter: 'dialogEnterAnimation',
  //   dialogLeave: 'dialogLeaveAnimation',
  // },
});
