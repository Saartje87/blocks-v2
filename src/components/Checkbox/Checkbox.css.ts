import { style } from '@vanilla-extract/css';
import { vars } from '../../css/theme.css';

export const input = style({
  all: 'unset',
});

export const checkbox = style({
  transitionDuration: `${vars.transition.duration.fast}, ${vars.transition.duration.normal}`,
  transitionProperty: 'background-color, box-shadow',
  selectors: {
    [`${input}:focus-visible + &`]: {
      outline: '2px solid transparent',
      outlineOffset: '2px',
      boxShadow: '0 0 0 3px rgba(66, 153, 225, 0.6)',
    },
    [`${input}:checked + &`]: {
      backgroundColor: vars.color.primary,
    },
  },
});

export const checkboxMark = style({
  transform: 'scale(0)',
  selectors: {
    [`${input}:checked + ${checkbox} &`]: {
      transform: 'scale(1)',
    },
  },
});
