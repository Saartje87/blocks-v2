import { FC, useState } from 'react';
import { IconButton } from '../IconButton/IconButton';
import { Input, InputProps } from '../Input';

export type PasswordProps = Omit<InputProps, 'type'>;

export const Password: FC<PasswordProps> = ({ ...props }) => {
  const [showPassword, setShowPassword] = useState(false);

  return (
    <Input
      type={showPassword ? 'text' : 'password'}
      endSlot={
        <IconButton
          icon={showPassword ? 'eye-slash' : 'eye'}
          size="medium"
          color="text"
          onClick={() => setShowPassword((shown) => !shown)}
          label={showPassword ? 'Hide password' : 'Show password'}
        />
      }
      {...props}
    />
  );
};
