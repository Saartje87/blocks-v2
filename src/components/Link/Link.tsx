import { ElementType, forwardRef } from 'react';
import {
  PolymorphicComponent,
  PolymorphicComponentProps,
  PolymorphicComponentRef,
} from '../../types/polymorphic';
import { classnames } from '../../utils/classnames';
import { useBlocksContext } from '../BlocksProvider/useBlocksContext';
import { useComponentStyles } from '../BlocksProvider/useComponentStyles';
import { Box, OwnBoxProps } from '../Box';

const defaultElement = 'a';

export type OwnLinkProps = {
  variant?: 'inherit' | 'primary' | 'secondary';
  underline?: boolean;
} & OwnBoxProps;

export type LinkProps<C extends ElementType = 'div'> = PolymorphicComponentProps<C, OwnLinkProps>;

// TODO Should we remove `const { linkComponent } = useBlocksContext();`
// and add doumentaion that you can use `as` prop to override the default element?
// And for reusability create a custom link component? <- add this to documentation

export const Link: PolymorphicComponent<OwnLinkProps> = forwardRef(function Link<
  C extends ElementType = typeof defaultElement,
>(
  { as, className, underline, variant = 'primary', ...restProps }: LinkProps<C>,
  ref: PolymorphicComponentRef<C>,
) {
  const { linkComponent } = useBlocksContext();
  const Component = as || linkComponent || defaultElement;
  const linkClassName = useComponentStyles('link', { base: true, variant });

  return (
    <Box
      as={Component}
      ref={ref}
      cursor="pointer"
      className={classnames(linkClassName, className)}
      textDecoration={underline ? 'underline' : 'none'}
      {...restProps}
    />
  );
});
