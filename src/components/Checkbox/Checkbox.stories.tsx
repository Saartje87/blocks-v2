import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { Stack } from '../Stack';
import { Checkbox, CheckboxProps } from './Checkbox';

export default {
  title: 'Form/Checkbox',
  component: Checkbox,
  argTypes: {},
} as Meta<typeof Checkbox>;

const Template: StoryFn<typeof Checkbox> = (args) => (
  <Stack gap="medium">
    <Checkbox {...args} name="foo" />
    <Checkbox label="Checkbox 2" name="bar" />
    <Checkbox label="Checkbox 3" name="baz" />
  </Stack>
);

export const Default: StoryObj<CheckboxProps> = {
  render: Template,
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    expect(canvas.getByText('Checkbox 1')).toBeInTheDocument();
  },
  args: {
    label: 'Checkbox 1',
  },
};
