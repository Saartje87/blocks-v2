import { FC, ReactNode, useMemo } from 'react';
import { atoms } from '../../css/sprinkles.css';
import { classnames } from '../../utils/classnames';
import { BlocksProviderContext, BlocksProviderContextValue } from './BlocksProviderContext';

export type BlocksProviderProps = {
  children: ReactNode;
} & BlocksProviderContextValue;

export const BlocksProvider: FC<BlocksProviderProps> = ({
  children,
  theme,
  spriteUrl,
  linkComponent,
}) => {
  const value = useMemo(
    (): BlocksProviderContextValue => ({ theme, spriteUrl, linkComponent }),
    [theme, spriteUrl, linkComponent],
  );

  return (
    <BlocksProviderContext.Provider value={value}>
      <div className={classnames(theme.vars, atoms({ fontFamily: 'body' }))}>{children}</div>
    </BlocksProviderContext.Provider>
  );
};
