import { style } from '@vanilla-extract/css';
import { atoms } from '../../css/sprinkles.css';
import { vars } from '../../css/theme.css';

export const input = style({
  opacity: 0,
  height: 0,
  maxHeight: 0,
  position: 'absolute',
});

export const radio = style({
  transitionDuration: `${vars.transition.duration.fast}, ${vars.transition.duration.normal}`,
  transitionProperty: 'background-color, box-shadow',
  selectors: {
    [`${input}:focus-visible + &`]: {
      outline: '2px solid transparent',
      outlineOffset: '2px',
      boxShadow: '0 0 0 3px rgba(66, 153, 225, 0.6)',
    },
    [`${input}:checked + &`]: {
      backgroundColor: vars.color.primary,
    },
  },
});

export const radioMark = style([
  atoms({
    backgroundColor: 'white',
  }),
  {
    width: vars.icon.size.small,
    height: vars.icon.size.small,
    borderRadius: '50%',
    transform: 'scale(0)',
    selectors: {
      [`${input}:checked + ${radio} &`]: {
        transform: 'scale(0.8)',
      },
    },
  },
]);
