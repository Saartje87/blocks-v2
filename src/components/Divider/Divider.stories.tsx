import { Meta, StoryFn } from '@storybook/react';
import { Stack } from '../Stack';
import { Text } from '../Text';
import { Divider } from './Divider';

export default {
  title: 'Layout/Divider',
  component: Divider,
  argTypes: {},
} as Meta<typeof Divider>;

const Template: StoryFn<typeof Divider> = (args) => (
  <Stack gap="medium">
    <Text>Foo</Text>
    <Divider {...args} />
    <Text>Bar</Text>
  </Stack>
);

export const Default = {
  render: Template,
  args: {},
};
