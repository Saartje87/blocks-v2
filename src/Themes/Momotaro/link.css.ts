import { style, styleVariants } from '@vanilla-extract/css';
import { createComponentTheme } from '../../components/BlocksProvider/createComponentTheme';

export const link = createComponentTheme('link', ({ atoms, vars }) => ({
  base: style({
    outline: 'none',
    border: 'none',
    textDecoration: 'none',
    background: 'transparent',
    ':hover': {
      textDecoration: 'underline',
    },
    ':focus-visible': {
      textDecoration: 'underline',
    },
  }),
  variant: styleVariants({
    inherit: {},
    primary: [
      atoms({
        color: 'primary',
        fontWeight: 'medium',
      }),
    ],
    secondary: [
      atoms({
        color: 'secondary',
        fontWeight: 'medium',
      }),
    ],
  }),
}));
