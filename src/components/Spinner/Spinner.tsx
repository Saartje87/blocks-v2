import { forwardRef } from 'react';
import { Atoms } from '../../css/sprinkles.css';
import { classnames } from '../../utils';
import { MarginAtoms } from '../../utils/css';
import { useComponentStyles } from '../BlocksProvider';
import { Box } from '../Box';
import * as styles from './Spinner.css';

export type SpinnerProps = {
  className?: string;
  size?: 'small' | 'medium' | 'large';
  color?: Atoms['color'];
} & MarginAtoms;

export const Spinner = forwardRef<HTMLDivElement, SpinnerProps>(function Spinner(
  { className, size = 'medium', color, ...restProps },
  ref,
) {
  const spinnerClassName = useComponentStyles('spinner', { size });

  return (
    <Box
      ref={ref}
      color={color}
      className={classnames(styles.spinner, spinnerClassName, className)}
      {...restProps}
    />
  );
});
