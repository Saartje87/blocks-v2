import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { Stack } from '../Stack';
import { Col } from './Col';
import { Row } from './Row';

export default {
  title: 'Layout/Grid',
  component: Row,
  argTypes: {},
} as Meta<typeof Row>;

const Template: StoryFn<typeof Row> = ({ children }) => <Stack gap="medium">{children}</Stack>;

const TemplateWithChildren: StoryFn<typeof Row> = ({ ...rowProps }) => (
  <Stack gap="medium">
    <Row {...rowProps}>
      <Col backgroundColor="secondary" textAlign="center" span={12}>
        12
      </Col>
    </Row>
    <Row {...rowProps}>
      <Col backgroundColor="secondary" textAlign="center" span={6}>
        6
      </Col>
      <Col backgroundColor="secondary" textAlign="center" span={6}>
        6
      </Col>
    </Row>
    <Row {...rowProps}>
      <Col backgroundColor="secondary" textAlign="center" span={4}>
        4
      </Col>
      <Col backgroundColor="secondary" textAlign="center" span={4}>
        4
      </Col>
      <Col backgroundColor="secondary" textAlign="center" span={4}>
        4
      </Col>
    </Row>
    <Row {...rowProps}>
      <Col backgroundColor="secondary" textAlign="center" span={4}>
        4
      </Col>
      <Col backgroundColor="secondary" textAlign="center" span={8}>
        8
      </Col>
    </Row>
    <Row {...rowProps}>
      <Col backgroundColor="secondary" textAlign="center" span={3}>
        3
      </Col>
      <Col backgroundColor="secondary" textAlign="center" span={3}>
        3
      </Col>
      <Col backgroundColor="secondary" textAlign="center" span={3}>
        3
      </Col>
      <Col backgroundColor="secondary" textAlign="center" span={3}>
        3
      </Col>
    </Row>
    <Row {...rowProps}>
      <Col backgroundColor="secondary" textAlign="center" span={2}>
        2
      </Col>
      <Col backgroundColor="secondary" textAlign="center" span={2} offset={8}>
        2
      </Col>
    </Row>
  </Stack>
);

export const Default: StoryObj = {
  render: TemplateWithChildren,
  args: {},

  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    expect(canvas.getByText('12')).toBeInTheDocument();
  },
};

export const Gap: StoryObj = {
  render: Template,

  args: {
    children: (
      <>
        <Row>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
        </Row>
        <Row gap={['xsmall', 'medium', 'xlarge']}>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
          <Col backgroundColor="secondary" textAlign="center" span={2}>
            2
          </Col>
        </Row>
      </>
    ),
  },
};

export const Responsive: StoryObj = {
  render: Template,

  args: {
    children: (
      <Row>
        <Col backgroundColor="secondary" textAlign="center" span={[6, 4]}>
          span [6, 4]
        </Col>
        <Col backgroundColor="secondary" textAlign="center" span={[6, 4]} offset={[0, 4]}>
          span [6, 4] offset [0, 4]
        </Col>
      </Row>
    ),
  },
};
