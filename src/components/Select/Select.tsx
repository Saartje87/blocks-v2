import { forwardRef, ReactElement, useId } from 'react';
import { HTMLProps } from '../../types/utils';
import { classnames } from '../../utils';
import { useComponentStyles } from '../BlocksProvider';
import { Box } from '../Box';
import { Icon } from '../Icon';
import { Label } from '../Label/Label';
import * as styles from './select.css';

export type SelectProps = {
  children: ReactElement<HTMLOptionElement> | ReactElement<HTMLOptionElement>[];
  className?: string;
  label?: string;
  name: string;
} & HTMLProps<HTMLSelectElement>;

export const Select = forwardRef<HTMLSelectElement, SelectProps>(function Select(
  { children, className, label, placeholder, name, ...props },
  ref,
) {
  const id = useId();
  const containerClassName = useComponentStyles('input', { container: true });
  const inputClassName = useComponentStyles('input', { input: true });

  return (
    <Box>
      {!!label && <Label htmlFor={id}>{label}</Label>}

      <Box
        display="flex"
        position="relative"
        overflow="hidden"
        alignItems="center"
        className={classnames(containerClassName, className)}
      >
        <select
          ref={ref}
          id={id}
          className={classnames(styles.select, inputClassName)}
          name={name}
          {...props}
        >
          {placeholder && (
            <option className={styles.placeholder} value="">
              {placeholder}
            </option>
          )}
          {children}
        </select>

        <Box
          display="flex"
          alignItems="center"
          justifyContent="center"
          position="absolute"
          top={0}
          right={0}
          bottom={0}
          paddingRight="large"
          paddingLeft="small"
          backgroundColor="white"
          pointerEvents="none"
        >
          <Icon icon="chevron-down" size="small" color="text" />
        </Box>
      </Box>
    </Box>
  );
});
